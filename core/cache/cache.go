package cache

import (
	"fmt"
	"time"
)

const (
	CacheRedisProvider  = "redis"
	CachememoryProvider = "in-memory"
)

type Options interface {
	GetProvider() string
}

type Cache interface {
	Get(string) CacheResult
	Set(string, interface{}, time.Duration) (bool, error)
	TTL(string) (time.Duration, error)
}

type CacheResult interface {
	Result() (string, error)
	Bytes() ([]byte, error)
	Bool() (bool, error)
	Int() (int, error)
	Float64() (float64, error)
}

func New(option Options) (Cache, error) {
	switch option.GetProvider() {
	case CacheRedisProvider:
		val, ok := option.(*RedisCacheOptions)
		if !ok {
			return nil, fmt.Errorf("invalid redis cache parameters")
		}
		return NewRedisCacheProvider(val)
	}

	return nil, fmt.Errorf("unable to identify the provider")
}
