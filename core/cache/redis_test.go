package cache_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/cache"
)

func TestRedisCacheImplementation(t *testing.T) {
	t.Run("init redis cache", func(t *testing.T) {
		redis, err := cache.New(&cache.RedisCacheOptions{})
		require.Nil(t, err)
		fmt.Println(redis)
	})
}
