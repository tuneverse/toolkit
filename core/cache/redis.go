package cache

import (
	"context"
	"time"

	"github.com/redis/go-redis/v9"
)

type RedisCacheProvider struct {
	client *redis.Client
	ctx    context.Context
}

// get the redis key
func (redis *RedisCacheProvider) get(key string) *redis.StringCmd {
	cmd := redis.client.Get(redis.ctx, key)
	return cmd
}

// get the data as string
func (redis *RedisCacheProvider) Get(key string) CacheResult {
	return &RedisCacheResult{
		cmd: redis.get(key), // get the data and store in the cmd
	}
}

// set the data with expiry time
func (redis *RedisCacheProvider) Set(key string, value interface{}, expiration time.Duration) (bool, error) {
	err := redis.client.Set(redis.ctx, key, value, expiration).Err()
	return err == nil, err
}

// Get the TTL of a key
func (redis *RedisCacheProvider) TTL(key string) (time.Duration, error) {
	duration, err := redis.client.TTL(redis.ctx, key).Result()
	if err != nil {
		return 0, err
	}
	return duration, nil
}

// initiate new redis provider
func NewRedisCacheProvider(option *RedisCacheOptions) (Cache, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     option.Host,
		Password: option.Password,
		DB:       option.DB,
	})

	return &RedisCacheProvider{
		client: rdb,
		ctx:    context.Background(),
	}, nil
}
