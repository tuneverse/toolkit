package cache

type RedisCacheOptions struct {
	Host     string
	UserName string
	Password string
	DB       int `json:"db" default:"0"`
}

func (redis *RedisCacheOptions) GetProvider() string {
	return CacheRedisProvider
}
