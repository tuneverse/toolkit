package cache

import (
	"github.com/redis/go-redis/v9"
)

type RedisCacheResult struct {
	cmd *redis.StringCmd
}

// get the data as string
func (redis *RedisCacheResult) Result() (string, error) {
	resp, err := redis.cmd.Result()
	return resp, IsRedisNil(err)
}

// get the data as bytes
func (redis *RedisCacheResult) Bytes() ([]byte, error) {
	resp, err := redis.cmd.Bytes()
	return resp, IsRedisNil(err)

}

// get the data as bool
func (redis *RedisCacheResult) Bool() (bool, error) {
	resp, err := redis.cmd.Bool()
	return resp, IsRedisNil(err)

}

// get the data as int
func (redis *RedisCacheResult) Int() (int, error) {
	resp, err := redis.cmd.Int()
	return resp, IsRedisNil(err)

}

// get the data as float64
func (redis *RedisCacheResult) Float64() (float64, error) {
	resp, err := redis.cmd.Float64()
	return resp, IsRedisNil(err)
}

func IsRedisNil(err error) error {
	if err == redis.Nil {
		return nil
	}
	return err
}
