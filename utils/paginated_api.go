package utils

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/tidwall/gjson"
)

type request struct {
	host    string
	headers map[string]interface{}
	body    map[string]interface{}
	method  string
}

type Options func(*request)

func NewRequest(opts ...Options) *request {
	var re = &request{}
	for _, opt := range opts {
		opt(re)
	}
	return re
}

func WithHost(host string) Options {
	return func(re *request) {
		re.host = host
	}
}

func WithMethod(method string) Options {
	return func(re *request) {
		re.method = method
	}
}

type APIResponseOutput struct {
	Method  string                 `json:"method"`
	Data    interface{}            `json:"data"`
	Headers map[string]interface{} `json:"headers"`
	Body    map[string]interface{} `json:"body"`
}

type Pagination struct {
	request *request
	Exec    func(int) (*http.Response, error)

	started  bool
	prevPage int
	nextPage int

	err  error
	data interface{}
}

func (p *Pagination) Next() bool {
	if !p.HasNextPage() {
		return false
	}

	resp, err := p.Exec(p.nextPage)
	if err != nil {
		p.err = err
		return false
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		p.err = err
		return false
	}

	paginationKey := "data.metadata"
	results := gjson.Get(string(body), paginationKey).Map()

	p.data = &APIResponseOutput{
		Method:  p.request.method,
		Data:    string(body),
		Headers: p.request.headers,
		Body:    p.request.body,
	}

	if v, ok := results["next"]; ok {
		p.prevPage = p.nextPage
		p.nextPage = int(v.Int())
	} else {
		p.nextPage = 0
	}
	return true
}

func (p *Pagination) HasNextPage() bool {
	return p.nextPage > 0
}

func (p *Pagination) Page() interface{} {
	return p.data
}
func (p *Pagination) Err() error {
	return p.err
}

func ApiRequestWithPages(ctx context.Context, request request, fn func(*APIResponseOutput, bool) bool) error {
	p := Pagination{
		nextPage: 1,
		request:  &request,
		Exec: func(page int) (*http.Response, error) {
			base, err := url.Parse(request.host)
			if err != nil {
				return nil, err
			}

			// Query params
			params := url.Values{}
			params.Add("page", fmt.Sprintf("%v", page))
			base.RawQuery = params.Encode()
			resp, err := APIRequest(request.method, base.String(), request.headers, request.body)
			if err != nil {
				return nil, err
			}
			return resp, nil
		},
	}

	for p.Next() {
		if !fn(p.Page().(*APIResponseOutput), !p.HasNextPage()) {
			break
		}
	}

	return p.Err()
}
