package utils

import "gitlab.com/tuneverse/toolkit/consts"

func TempDir() string {
	return consts.DefaultDirName
}
