## Overview
This module deals with function for validating UUIDs.
## Index
- [IsValidUUID(input string) bool 
    ](#func-IsValidUUID)

- [IsValidUUID(input string) bool](#func-IsValidUUID)

### func IsValidUUID

    IsValidUUID(input string) bool 

    This function is used for validating UUIDs.

